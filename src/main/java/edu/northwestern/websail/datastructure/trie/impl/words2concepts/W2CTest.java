package edu.northwestern.websail.datastructure.trie.impl.words2concepts;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import edu.northwestern.websail.datastructure.trie.impl.words2concepts.utils.W2CTrieSerializer;

/*
 * @author csbhagav
 */
public class W2CTest {

	/**
	 * @param args
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public static void main(String[] args) throws FileNotFoundException,
			IOException {

		String fileDirectory = args[0];

		// create a Trie object
		W2CTrie wtcTrie = new W2CTrie();
		// testing
		// preparing data to test
		// String[] sentences = { "Presidents of the United States",
		// "Presidents of India", "Presidents of the United States army",
		// "Northwestern University",
		// "Northwestern University of the United States",
		// "Presidents of the United States" };
		String[] sentences = { "Presidents of the United States",
				"Presidents of the United StateS",
				"Presidents of the United States", "Individual psychology",
				"Individual PSychology" };

		// add each sentence to the Trie
		// add method needs
		// - array of strings (word)
		// - integer (wikipedia page id associated with the strings)
		for (int i = 0; i < sentences.length; i++) {
			wtcTrie.addMention(sentences[i], i, 0.01 * (i + 1),
					W2CConstants.INTERNAL_LINKS, true);
		}

		// retrieving data back
		for (int i = 0; i < sentences.length; i++) {
			System.out.println(wtcTrie.getCandidates(sentences[i], false));
		}

		/*
		 * 
		 * ***
		 * Using Custom serializer. ***
		 */
		BufferedWriter out = new BufferedWriter(new FileWriter(new File(
				fileDirectory + "/data")));
		W2CTrieSerializer.writeToFile(out, wtcTrie);
		out.close();

		BufferedReader in = new BufferedReader(new FileReader(new File(
				fileDirectory + "/data")));
		W2CTrie t2 = W2CTrieSerializer.readFromFile(in);
		out = new BufferedWriter(new FileWriter(new File(fileDirectory
				+ "/data2")));
		W2CTrieSerializer.writeToFile(out, t2);
		out.close();

		// try to get node that's not in the Trie
		// should return null
		// System.out.println(root.getNodeFromSentence(root.getRootNode(),
		// "individual psychology".split(" ")));

	}

}
