package edu.northwestern.websail.datastructure.trie.ds;

import java.util.HashMap;

public class Node<V, E, C> {

	Integer id;
	V value;
	HashMap<E, Node<V, E, C>> children = new HashMap<E, Node<V, E, C>>();
	Node<V, E, C> parent;
	C endpoint;

	public Node(int id) {
		this.id = id;
	}

	public Node(int id, V val) {
		this.id = id;
		this.value = val;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public V getValue() {
		return value;
	}

	public void setValue(V value) {
		this.value = value;
	}

	public void addChild(E key, Node<V, E, C> n) {
		children.put(key, n);
		n.setParent(this);
	}

	public Node<V, E, C> getChild(E key) {
		return children.get(key);
	}

	public boolean hasChild(E key) {
		return children.containsKey(key);
	}

	public HashMap<E, Node<V, E, C>> getChildren() {
		return children;
	}

	public void setChildren(HashMap<E, Node<V, E, C>> children) {
		this.children = children;
	}

	public C getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(C endpoint) {
		this.endpoint = endpoint;
	}

	public Node<V, E, C> getParent() {
		return parent;
	}

	public void setParent(Node<V, E, C> parent) {
		this.parent = parent;
	}

	public String toString() {
		String serialStr = "";
		serialStr += this.id + "\t";
		if (value != null)
			serialStr += value.toString() + "\t";
		else
			serialStr += "null\t";

		if (endpoint == null)
			serialStr += "false";
		else
			serialStr += "true\t" + endpoint.toString();

		return serialStr.trim();
	}

}
