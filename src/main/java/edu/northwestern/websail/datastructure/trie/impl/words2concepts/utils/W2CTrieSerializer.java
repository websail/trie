package edu.northwestern.websail.datastructure.trie.impl.words2concepts.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Queue;

import edu.northwestern.websail.datastructure.trie.ds.Node;
import edu.northwestern.websail.datastructure.trie.ds.Trie;
import edu.northwestern.websail.datastructure.trie.ds.TrieEdge;
import edu.northwestern.websail.datastructure.trie.impl.words2concepts.W2CMentionNode;
import edu.northwestern.websail.datastructure.trie.impl.words2concepts.W2CTrie;

public class W2CTrieSerializer {

	public static void writeToFile(BufferedWriter out, W2CTrie w2cTrie)
			throws IOException {

		Trie<String, String, W2CMentionNode> trie = w2cTrie.getTrie();
		Node<String, String, W2CMentionNode> root = trie.getRoot();
		ArrayList<TrieEdge> edgeList = new ArrayList<TrieEdge>();

		System.out.println("Number of nodes = " + trie.getNumNodes());
		out.write("==Nodes==\n");
		dfsWriteTrieNodes(out, root, edgeList);

		out.write("==Edges==\n");
		for (TrieEdge e : edgeList) {
			out.write(e.getId() + "\t" + e.getParentId() + "\t"
					+ e.getEdgeIdentifier() + "\n");
		}
		out.flush();
		out.close();
	}

	private static void dfsWriteTrieNodes(BufferedWriter out,
			Node<String, String, W2CMentionNode> root,
			ArrayList<TrieEdge> edgeList) throws IOException {

		Queue<Node<String, String, W2CMentionNode>> queue = new LinkedList<Node<String, String, W2CMentionNode>>();
		HashMap<String, Node<String, String, W2CMentionNode>> children;
		Node<String, String, W2CMentionNode> n;
		queue.add(root);

		while (!queue.isEmpty()) {
			n = queue.poll();
			out.write(n.toString() + "\n");
			children = n.getChildren();
			for (Entry<String, Node<String, String, W2CMentionNode>> e : children
					.entrySet()) {

				edgeList.add(new TrieEdge(e.getValue().getId(), n.getId(), e
						.getKey()));
				queue.add(e.getValue());

			}

		}

	}

	public static W2CTrie readFromFile(BufferedReader in) throws IOException {

		HashMap<Integer, Node<String, String, W2CMentionNode>> nodes = new HashMap<Integer, Node<String, String, W2CMentionNode>>();

		String line = "";
		// Read all nodes and mentions + candidates
		while ((line = in.readLine()) != null) {
			if (line.equalsIgnoreCase("==Edges==")) {
				break;
			} else if (line.equalsIgnoreCase("==Nodes=="))
				continue;

			String[] parts = line.split("\t");

			String val = new String(parts[1]);
			Node<String, String, W2CMentionNode> n = new Node<String, String, W2CMentionNode>(
					Integer.valueOf(parts[0]), val);
			if (parts[2].equalsIgnoreCase("true")) {

				W2CMentionNode mention = new W2CMentionNode();

				for (int i = 3; i < parts.length; i += 2) {

					String key = parts[i];
					String candidatesSet = parts[i + 1];

					mention.addCandidateFromSerializedString(key, candidatesSet);
				}
				n.setEndpoint(mention);
			}

			nodes.put(n.getId(), n);
		}

		// Read all Edges
		int childId = 0;
		int parentId = 0;
		String edgeIdentifier = "";
		while ((line = in.readLine()) != null) {

			String[] parts = line.split("\t");
			childId = Integer.valueOf(parts[0]);
			parentId = Integer.valueOf(parts[1]);
			edgeIdentifier = parts[2];

			nodes.get(childId).setParent(nodes.get(parentId));
			nodes.get(parentId).addChild(edgeIdentifier, nodes.get(childId));

		}

		Trie<String, String, W2CMentionNode> trie = new Trie<String, String, W2CMentionNode>();
		trie.setRoot(nodes.get(0));
		W2CTrie t = new W2CTrie();
		t.setTrie(trie);
		trie.setNumNodes(nodes.size());
		in.close();
		return t;

	}
}

