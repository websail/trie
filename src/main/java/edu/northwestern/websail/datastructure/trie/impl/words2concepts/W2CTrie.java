package edu.northwestern.websail.datastructure.trie.impl.words2concepts;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.TreeSet;

import edu.northwestern.websail.datastructure.trie.ds.Node;
import edu.northwestern.websail.datastructure.trie.ds.Trie;
import edu.northwestern.websail.datastructure.trie.impl.words2concepts.utils.W2CCandidateComparator;

public class W2CTrie implements Serializable {

	private static final long serialVersionUID = -6990870876467910271L;
	protected Trie<String, String, W2CMentionNode> trie;

	public W2CTrie() {
		trie = new Trie<String, String, W2CMentionNode>();
	}

	public void addMention(String surfaceForm, int mentionPageId, Double prob,
			Byte source, boolean ignoreNodeValue) {

		String[] sentenceWords = surfaceForm.split(" ");
		int len = sentenceWords.length;

		String[] edgeIdentifiers = new String[len];
		String[] nodes = new String[len];

		for (int i = 0; i < len; i++) {
			edgeIdentifiers[i] = sentenceWords[i].toLowerCase();
			if (ignoreNodeValue)
				nodes[i] = null;
			else
				nodes[i] = new String(sentenceWords[i]);
		}

		Node<String, String, W2CMentionNode> current = trie.insertNodes(nodes,
				edgeIdentifiers);

		if (current.getEndpoint() == null)
			current.setEndpoint(new W2CMentionNode());

		current.getEndpoint().addCandidate(surfaceForm,
				new W2CCandidate(mentionPageId, prob, source));

	}

	public TreeSet<W2CCandidate> getCandidates(String surfaceForm,
			boolean foldCase) {

		String[] sentenceWords = surfaceForm.split(" ");
		int len = sentenceWords.length;
		String[] edgeIdentifiers = new String[len];

		for (int i = 0; i < len; i++) {
			edgeIdentifiers[i] = sentenceWords[i].toLowerCase();
		}

		Node<String, String, W2CMentionNode> n = trie.getNode(edgeIdentifiers);

		TreeSet<W2CCandidate> result = new TreeSet<W2CCandidate>(
				new W2CCandidateComparator());

		if (n != null) {
			HashMap<String, TreeSet<W2CCandidate>> candidates = n.getEndpoint()
					.getCandidates();
			if (!foldCase)
				return candidates.get(surfaceForm);
			else {
				for (Entry<String, TreeSet<W2CCandidate>> e : candidates
						.entrySet())
					result.addAll(e.getValue());
			}
		}

		return result;

	}

	public TreeSet<W2CCandidate> getCandidatesFromNodeWithCaseFolding(
			Node<String, String, W2CMentionNode> n) {

		TreeSet<W2CCandidate> result = new TreeSet<W2CCandidate>(
				new W2CCandidateComparator());

		if (n != null) {
			HashMap<String, TreeSet<W2CCandidate>> candidates = n.getEndpoint()
					.getCandidates();
			for (Entry<String, TreeSet<W2CCandidate>> e : candidates.entrySet())
				result.addAll(e.getValue());
		}

		return result;
	}

	public Trie<String, String, W2CMentionNode> getTrie() {
		return trie;
	}

	public void setTrie(Trie<String, String, W2CMentionNode> trie) {
		this.trie = trie;
	}

}
