package edu.northwestern.websail.datastructure.trie.impl.w2cSQL.candidateComparators;

import edu.northwestern.websail.datastructure.trie.impl.w2cSQL.W2CSQLCandidate;

public class W2CSQLCandidateExternalProbComparator implements
		W2CSQLTrieComparatorImpl {

	public int compare(W2CSQLCandidate arg0, W2CSQLCandidate arg1) {
		int sign = (int) Math.signum(arg1.getProbExternal()
				- arg0.getProbExternal());
		if (sign == 0)
			sign = (int) Math.signum(arg1.getProbInternal()
					- arg0.getProbInternal());

		if (sign == 0) {
			return arg0.getConceptId() - arg1.getConceptId();
		} else {
			return sign;
		}
	}

}
