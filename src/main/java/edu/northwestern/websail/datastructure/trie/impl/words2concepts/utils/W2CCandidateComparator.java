package edu.northwestern.websail.datastructure.trie.impl.words2concepts.utils;

import java.util.Comparator;

import edu.northwestern.websail.datastructure.trie.impl.words2concepts.W2CCandidate;

public class W2CCandidateComparator implements Comparator<W2CCandidate> {

	public int compare(W2CCandidate o1, W2CCandidate o2) {

		int sign = (int) Math.signum(o2.getProbabilityPageGivenMention()
				- o1.getProbabilityPageGivenMention());
		if (sign == 0) {
			return o1.getPageID() - o2.getPageID();
		} else
			return sign;
	}

}
