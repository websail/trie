package edu.northwestern.websail.datastructure.trie.impl.w2cSQL.candidateComparators;

import java.util.Comparator;

import edu.northwestern.websail.datastructure.trie.impl.w2cSQL.W2CSQLCandidate;

public interface W2CSQLTrieComparatorImpl extends Comparator<W2CSQLCandidate> {

}
