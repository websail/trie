package edu.northwestern.websail.datastructure.trie.impl.w2cSQL.utils;

/**
 * @author NorThanapon
 * Only cache 
 * getCandidatesFromJointTable
 * mapMention
 * mapEdge
 * 
 * sqllimit is now a property
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import edu.northwestern.websail.datastructure.trie.impl.w2cSQL.W2CSQLCandidate;

public class W2CSQLSurfaceCacheDA extends W2CSQLDataAccess {
	public static final int surfaceSize = 132158174;
	private final LoadingCache<Integer, TreeSet<W2CSQLCandidate>> candidateCache;
	private final String[] surfaceIdCache;
	private final HashMap<String, Integer> edgeCache;
	private long candidateAccDB = 0;
	private long candidateAccTotal = 0;
	private Connection[] connPool;
	private int[] connUsage;
	private int sqlLimit;
	private String surfaceMapFilename = "surface_map";
	private Random rand = new Random();

	public W2CSQLSurfaceCacheDA(Connection conn, Connection[] connPool,
			int cacheSize, int sqlLimit) throws SQLException, IOException {
		super(conn);
		this.connPool = connPool;
		this.connUsage = new int[connPool.length];
		for (int i = 0; i < connPool.length; i++) {
			connUsage[i] = 0;
		}
		this.sqlLimit = sqlLimit;
		candidateCache = CacheBuilder.newBuilder().maximumSize(cacheSize)
				.expireAfterAccess(3, TimeUnit.DAYS)
				.build(new CacheLoader<Integer, TreeSet<W2CSQLCandidate>>() {

					@Override
					public TreeSet<W2CSQLCandidate> load(Integer key)
							throws Exception {
						return loadCandidates(key);
					}

				});
		surfaceIdCache = new String[surfaceSize + 1];
		edgeCache = new HashMap<String, Integer>(5000000);
		this.loadSurfaces();
		this.loadEdges();
		logger.info("Construct " + W2CSQLSurfaceCacheDA.class.getName()
				+ " with " + connPool.length + "connections and " + cacheSize
				+ " cache size");
	}

	public W2CSQLSurfaceCacheDA(Connection conn, Connection[] connPool,
			String surfaceFilename, int cacheSize, int sqlLimit)
			throws SQLException, IOException {
		super(conn);
		this.connPool = connPool;
		this.connUsage = new int[connPool.length];
		for (int i = 0; i < connPool.length; i++) {
			connUsage[i] = 0;
		}
		this.sqlLimit = sqlLimit;
		candidateCache = CacheBuilder.newBuilder().maximumSize(cacheSize)
				.expireAfterAccess(3, TimeUnit.DAYS)
				.build(new CacheLoader<Integer, TreeSet<W2CSQLCandidate>>() {

					@Override
					public TreeSet<W2CSQLCandidate> load(Integer key)
							throws Exception {
						return loadCandidates(key);
					}

				});
		this.surfaceMapFilename = surfaceFilename;
		surfaceIdCache = new String[surfaceSize + 1];
		edgeCache = new HashMap<String, Integer>(5000000);
		this.loadSurfaces();
		this.loadEdges();
		logger.info("Construct " + W2CSQLSurfaceCacheDA.class.getName()
				+ " with " + connPool.length + "connections and " + cacheSize
				+ " cache size");

	}

	public W2CSQLSurfaceCacheDA(Connection conn, Connection[] connPool,
			HashMap<String, Integer> edgeCache, String[] surfaceIdCache,
			int cacheSize, int sqlLimit) throws SQLException, IOException {
		super(conn);
		this.connPool = connPool;
		this.connUsage = new int[connPool.length];
		for (int i = 0; i < connPool.length; i++) {
			connUsage[i] = 0;
		}
		this.sqlLimit = sqlLimit;
		candidateCache = CacheBuilder.newBuilder().maximumSize(cacheSize)
				.expireAfterAccess(3, TimeUnit.DAYS)
				.build(new CacheLoader<Integer, TreeSet<W2CSQLCandidate>>() {

					@Override
					public TreeSet<W2CSQLCandidate> load(Integer key)
							throws Exception {
						return loadCandidates(key);
					}

				});
		this.surfaceIdCache = surfaceIdCache;
		this.edgeCache = edgeCache;
		logger.info("Construct " + W2CSQLSurfaceCacheDA.class.getName()
				+ " with " + connPool.length + "connections and " + cacheSize
				+ " cache size");

	}

	@Override
	public TreeSet<W2CSQLCandidate> getCandidates(Integer surfaceId,
			int sqlLimit) {
		this.candidateAccTotal++;
		logger.fine("Try to get candidates for surface Id:" + surfaceId);
		if (candidateAccTotal % 10000 == 0) {
			logger.info("Candidate Cache statistic: " + this.candidateAccDB
					+ "/" + this.candidateAccTotal + " = "
					+ this.candidateAccDB / (double) this.candidateAccTotal);
		}
		return this.candidateCache.getUnchecked(surfaceId);
	}

	@Override
	public TreeSet<W2CSQLCandidate> getCandidates(Integer surfaceId,
			int sqlLimit, Connection conn) {
		return this.getCandidates(surfaceId, sqlLimit);
	}

	@Override
	public String mapMention(Integer key, Connection conn) throws SQLException {
		return this.surfaceIdCache[key];
	}

	@Override
	public Integer mapEdge(String value, Connection conn) throws SQLException {
		return this.edgeCache.get(value);
	}

	@Override
	public String mapMention(Integer key) throws SQLException {
		return this.surfaceIdCache[key];
	}

	@Override
	public Integer mapEdge(String value) throws SQLException {
		return this.edgeCache.get(value);
	}

	private int getConnectionIndex() {
		int min = connUsage[0];
		int minIndex = 0;
		ArrayList<Integer> availableIndexes = new ArrayList<Integer>();
		synchronized (connUsage) {
			for (int i = 0; i < connUsage.length; i++) {
				if (connUsage[i] <= min) {
					min = connUsage[i];
				}
			}
			for (int i = 0; i < connUsage.length; i++) {
				if (connUsage[i] <= min) {
					availableIndexes.add(i);
				}
			}
			minIndex = availableIndexes.get(rand.nextInt(availableIndexes
					.size()));
			connUsage[minIndex]++;
		}
		return minIndex;
	}

	private final void loadEdges() throws SQLException {
		logger.info("Loading Edge map");
		int index = this.getConnectionIndex();
		Connection conn = this.connPool[index];
		PreparedStatement edgeIdMapPS = conn.prepareStatement(
				"select * from w2ctrie_edge_map", ResultSet.TYPE_FORWARD_ONLY,
				ResultSet.CONCUR_READ_ONLY);
		edgeIdMapPS.setFetchSize(Integer.MIN_VALUE);

		ResultSet edgeIdMapRS = edgeIdMapPS.executeQuery();

		while (edgeIdMapRS.next()) {
			try {
				edgeCache.put(
						new String(edgeIdMapRS.getBytes("value"), "UTF-8"),
						edgeIdMapRS.getInt("id"));
			} catch (UnsupportedEncodingException e) {
				logger.severe("Unsupported Unicode");
				e.printStackTrace();
			}
		}
		edgeIdMapPS.close();
		edgeIdMapRS.close();
		logger.info("Done with " + this.edgeCache.size() + " edges");
	}

	private final void loadSurfaces() throws IOException, SQLException {
		File f = new File(this.surfaceMapFilename);
		if (f.exists()) {
			this.loadSurfacesFile(this.surfaceMapFilename);
		} else {
			this.loadSurfacesSQL();
		}
	}

	private void loadSurfacesFile(String filename) throws IOException {
		logger.info("Loading Surface map from File");

		BufferedReader in = new BufferedReader(new FileReader(filename));
		String line = null;
		String splitTAB = "\t";
		while ((line = in.readLine()) != null) {
			String[] parts = line.split(splitTAB);
			surfaceIdCache[Integer.parseInt(parts[0])] = parts[1];
		}
		in.close();
		logger.info("Done with " + this.surfaceIdCache.length + " surfaces");
	}

	private void loadSurfacesSQL() throws SQLException {
		logger.info("Loading Surface map from DB");
		int index = this.getConnectionIndex();
		Connection conn = this.connPool[index];
		PreparedStatement surfaceIdMapPS = conn.prepareStatement(
				"select * from w2ctrie_surface_forms_map",
				ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
		surfaceIdMapPS.setFetchSize(Integer.MIN_VALUE);

		ResultSet surfaceIdMapRS = surfaceIdMapPS.executeQuery();

		while (surfaceIdMapRS.next()) {
			try {
				surfaceIdCache[surfaceIdMapRS.getInt("id")] = new String(
						surfaceIdMapRS.getBytes("value"), "UTF-8");
			} catch (UnsupportedEncodingException e) {
				logger.severe("Unsupported Unicode");
				e.printStackTrace();
			}
		}
		surfaceIdMapPS.close();
		surfaceIdMapRS.close();
		logger.info("Done with " + this.surfaceIdCache.length + " surfaces");
	}

	private TreeSet<W2CSQLCandidate> loadCandidates(Integer key)
			throws SQLException {
		this.candidateAccDB++;
		int index = this.getConnectionIndex();
		Connection conn = this.connPool[index];
		TreeSet<W2CSQLCandidate> c = null;
		synchronized (conn) {
			try {
				c = super.getCandidates(key, sqlLimit, conn);
			} catch (com.mysql.jdbc.CommunicationsException e) {
				logger.warning("Database connection has became stale or closed. Attempting to issue query again...");
				c = super.getCandidates(key, sqlLimit, conn);
			}
		}
		synchronized (connUsage) {
			connUsage[index]--;
		}
		if (c == null) {
			logger.warning("Access DB for candidates of surface Id:" + key
					+ ", get null.");
			return new TreeSet<W2CSQLCandidate>();
		} else {
			logger.fine("Access DB for candidates of surface Id:" + key
					+ ", get " + c.size() + " candidates.");
			return c;
		}
	}
}
