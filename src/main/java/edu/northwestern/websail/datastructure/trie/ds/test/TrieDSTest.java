package edu.northwestern.websail.datastructure.trie.ds.test;

import edu.northwestern.websail.datastructure.trie.ds.Trie;

public class TrieDSTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Trie<String, String, String> t = new Trie<String, String, String>();

		t.insertNodes(new String[] { "List", "of", "U.S", "Presidents" },
				new String[] { "list", "of", "u.s", "presidents" });
		t.insertNodes(new String[] { "List", "of", "Indian", "Presidents" },
				new String[] { "list", "of", "indian", "presidents" });
		t.insertNodes(new String[] { "List", "of", "U.S", "Army" },
				new String[] { "list", "of", "u.s", "army" });

		System.out.println(t.getNode(
				new String[] { "list", "of", "u.s", "army" }).getId());
		System.out.println(t.getNode(new String[]{"list"}).getChildren());
		
		
		
		

	}

}
