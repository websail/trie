package edu.northwestern.websail.datastructure.trie.impl.w2cSQL.candidateComparators;

import edu.northwestern.websail.datastructure.trie.impl.w2cSQL.W2CSQLCandidate;

/**
 * @author csbhagav on 6/1/14.
 */
public class W2CSQLCandidateProbKeyComparator  implements
        W2CSQLTrieComparatorImpl{

    @Override
    public int compare(W2CSQLCandidate arg0, W2CSQLCandidate arg1) {
        int sign = (int) Math.signum(arg1.getProbSortKey()
                - arg0.getProbSortKey());
        if (sign == 0)
            sign = (int) Math.signum(arg1.getProbSortKey()
                    - arg0.getProbSortKey());

        if (sign == 0) {
            return arg0.getConceptId() - arg1.getConceptId();
        } else {
            return sign;
        }
    }
}
