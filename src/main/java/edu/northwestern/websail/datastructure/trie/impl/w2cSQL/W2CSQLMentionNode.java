/**
 * @author NorThanapon
 */
package edu.northwestern.websail.datastructure.trie.impl.w2cSQL;

import java.io.Serializable;
import java.util.HashSet;

public class W2CSQLMentionNode implements Serializable{

	private static final long serialVersionUID = -8180822531841903327L;
	private HashSet<Integer> mentionKeys;
	private int numberOfConcepts;
	private int numberOfLinks;
	public W2CSQLMentionNode(){
		super();
		this.setMentionKeys(new HashSet<Integer>());
	}
	
	public W2CSQLMentionNode(String serializedString) {
		super();
		this.setMentionKeys(new HashSet<Integer>());
		String[] parts = serializedString.split(":");
		this.setNumberOfConcepts(Integer.parseInt(parts[1]));
		this.setNumberOfConcepts(Integer.parseInt(parts[2]));
		parts = parts[0].split("\t");
		for (int i = 0; i < parts.length; i++) {
			this.mentionKeys.add(Integer.valueOf(parts[i]));
		}
	}
	
	public void addMention(Integer mentionKey){
		this.mentionKeys.add(mentionKey);
	}
	
	public Integer[] getMentionArray() {
		return this.mentionKeys.toArray(new Integer[this.mentionKeys.size()]);
	}
	
	@Override
	public String toString() {
		String retStr = "";
		for(Integer key : mentionKeys) {
			retStr += key + "\t";
		}
		return retStr + ":" + this.numberOfConcepts + ":"+this.numberOfLinks;
	}
	
	//getters and setters
	public HashSet<Integer> getMentionKeys() {
		return mentionKeys;
	}

	public void setMentionKeys(HashSet<Integer> mentionKeys) {
		this.mentionKeys = mentionKeys;
	}

	public int getNumberOfConcepts() {
		return numberOfConcepts;
	}

	public void setNumberOfConcepts(int numberOfConcepts) {
		this.numberOfConcepts = numberOfConcepts;
	}

	public int getNumberOfLinks() {
		return numberOfLinks;
	}

	public void setNumberOfLinks(int numberOfLinks) {
		this.numberOfLinks = numberOfLinks;
	}		
	
	
}
