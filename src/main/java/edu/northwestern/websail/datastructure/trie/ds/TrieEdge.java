package edu.northwestern.websail.datastructure.trie.ds;

public class TrieEdge {
	Integer id;
	Integer parentId;
	String edgeIdentifier;

	public TrieEdge(Integer id, Integer parentId, String edgeIdentifier) {
		super();
		this.id = id;
		this.parentId = parentId;
		this.edgeIdentifier = edgeIdentifier;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public String getEdgeIdentifier() {
		return edgeIdentifier;
	}

	public void setEdgeIdentifier(String edgeIdentifier) {
		this.edgeIdentifier = edgeIdentifier;
	}
}
