package edu.northwestern.websail.datastructure.trie.impl.words2concepts;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.TreeSet;

import edu.northwestern.websail.datastructure.trie.impl.words2concepts.utils.W2CCandidateComparator;

public class W2CMentionNode implements Serializable {

	private static final long serialVersionUID = 7551979667667979727L;
	private HashMap<String, TreeSet<W2CCandidate>> candidates;

	public W2CMentionNode() {
		this.candidates = new HashMap<String, TreeSet<W2CCandidate>>();
	}

	public HashMap<String, TreeSet<W2CCandidate>> getCandidates() {
		return candidates;
	}

	public void setCandidates(
			HashMap<String, TreeSet<W2CCandidate>> candidates) {
		this.candidates = candidates;
	}

	public void addCandidate(String key, W2CCandidate c) {
		if (!candidates.containsKey(key))
			candidates.put(key, new TreeSet<W2CCandidate>(
					new W2CCandidateComparator()));
		candidates.get(key).add(c);
	}

	public void addCandidateFromSerializedString(String key,
			String serializedCandidateSet) {

		TreeSet<W2CCandidate> cSet = new TreeSet<W2CCandidate>(
				new W2CCandidateComparator());
		serializedCandidateSet = serializedCandidateSet
				.replaceAll("\\[|\\]", "");

		String[] candidatesE = serializedCandidateSet.split(",");
		for (int i = 0; i < candidatesE.length; i++) {
			cSet.add(new W2CCandidate(candidatesE[i].trim()));
		}

		candidates.put(key, cSet);

	}

	public String toString() {

		String retStr = "";

		for (Entry<String, TreeSet<W2CCandidate>> e : candidates
				.entrySet()) {

			retStr += e.getKey() + "\t" + e.getValue() + "\t";

		}

		return retStr;
	}

}
