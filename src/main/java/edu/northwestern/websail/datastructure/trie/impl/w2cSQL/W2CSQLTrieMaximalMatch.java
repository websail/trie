package edu.northwestern.websail.datastructure.trie.impl.w2cSQL;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.TreeSet;

import edu.northwestern.websail.datastructure.trie.impl.w2cSQL.candidateComparators.W2CSQLCandidateExternalProbComparator;

public class W2CSQLTrieMaximalMatch {

	Integer startIdx;
	Integer endIdx;
	HashMap<String, TreeSet<W2CSQLCandidate>> allCandidates;

	public W2CSQLTrieMaximalMatch(Integer startIdx, Integer endIdx) {
		super();
		this.startIdx = startIdx;
		this.endIdx = endIdx;
		this.allCandidates = new HashMap<String, TreeSet<W2CSQLCandidate>>();
	}

	public Integer getStartIdx() {
		return startIdx;
	}

	public void setStartIdx(Integer startIdx) {
		this.startIdx = startIdx;
	}

	public Integer getEndIdx() {
		return endIdx;
	}

	public void setEndIdx(Integer endIdx) {
		this.endIdx = endIdx;
	}

	public HashMap<String, TreeSet<W2CSQLCandidate>> getAllCandidates() {
		return allCandidates;
	}

	public void setAllCandidates(
			HashMap<String, TreeSet<W2CSQLCandidate>> allCandidates) {

		W2CSQLCandidateExternalProbComparator comparator = new W2CSQLCandidateExternalProbComparator();

		for (Entry<String, TreeSet<W2CSQLCandidate>> e : allCandidates
				.entrySet()) {
			if (e.getValue().comparator() == null) {
				TreeSet<W2CSQLCandidate> t = new TreeSet<W2CSQLCandidate>(
						comparator);
				t.addAll(e.getValue());
				this.allCandidates.put(e.getKey(), t);
			} else {
				this.allCandidates.put(e.getKey(), e.getValue());
			}

		}
	}

	public void addMentionStrCandidatePair(String surface,
			TreeSet<W2CSQLCandidate> candidates) {

		if (candidates == null || candidates.size() == 0)
			return;

		this.allCandidates.put(surface, candidates);
	}

	public void addMentionStrCandidatePair(String surface,
			TreeSet<W2CSQLCandidate> candidates, int limit) {

		if (candidates == null || candidates.size() == 0)
			return;

		TreeSet<W2CSQLCandidate> candidateSubset = new TreeSet<W2CSQLCandidate>(
				candidates.comparator());
		int i = 0;
		for (W2CSQLCandidate c : candidates) {
			if (i < limit) {
				candidateSubset.add(c);
			} else {
				break;
			}
			i++;
		}

		this.allCandidates.put(surface, candidateSubset);
	}
}
