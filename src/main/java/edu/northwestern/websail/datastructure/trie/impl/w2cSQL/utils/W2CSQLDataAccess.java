/**
 * @author NorThanapon
 */
package edu.northwestern.websail.datastructure.trie.impl.w2cSQL.utils;

import edu.northwestern.websail.datastructure.trie.impl.w2cSQL.W2CSQLCandidate;
import edu.northwestern.websail.datastructure.trie.impl.w2cSQL.candidateComparators.W2CSQLCandidateExternalProbComparator;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.TreeSet;
import java.util.logging.Logger;

public class W2CSQLDataAccess {
    public static final Logger logger = Logger.getLogger(W2CSQLDataAccess.class.getName());
    private Integer nextEdgeKey = 1;
    private Integer nextMentionKey = 1;

    private Connection conn;

    public W2CSQLDataAccess(Connection conn)
            throws SQLException {
        this.conn = conn;
        this.nextEdgeKey = this.getLatestKey(EDGE_TABLE_NAME) + 1;
        this.nextMentionKey = this.getLatestKey(MENTION_TABLE_NAME) + 1;
    }

    public Integer[] mapEdges(String[] edges, boolean insertNew)
            throws SQLException {
        return this.mapEdges(edges, insertNew, this.conn);
    }

    public Integer[] mapEdges(String[] edges, boolean insertNew, Connection conn)
            throws SQLException {
        return this.map2Keys(edges, EDGE_TABLE_NAME, insertNew, conn);
    }

    public Integer[] mapMentions(String[] mentions, boolean insertNew)
            throws SQLException {
        return this.map2Keys(mentions, MENTION_TABLE_NAME, insertNew);
    }

    public String[] mapEdges(Integer[] keys) throws SQLException {
        return this.map2Values(keys, EDGE_TABLE_NAME);
    }

    public String[] mapMentions(Integer[] keys) throws SQLException {
        return this.map2Values(keys, MENTION_TABLE_NAME);
    }

    private Integer[] map2Keys(String[] sentence, String tableName,
                               boolean insertNew) throws SQLException {
        return this.map2Keys(sentence, tableName, insertNew, this.conn);
    }

    private Integer[] map2Keys(String[] sentence, String tableName,
                               boolean insertNew, Connection conn) throws SQLException {
        String mapSQL = null;
        if (tableName.equals(this.EDGE_TABLE_NAME)) {
            mapSQL = this.GET_EDGE_KEYS_SQL;
        } else {
            mapSQL = this.GET_MENTION_KEYS_SQL;
        }
        Integer[] keys = new Integer[sentence.length];
        String query = this.prepareINClause(mapSQL, "value", keys.length);
        int j = 1;
        PreparedStatement statement = conn.prepareStatement(query);
        for (String value : sentence) {
            statement.setString(j, value);
            j++;
        }
        ResultSet result = statement.executeQuery();
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        while (result.next()) {
            map.put(result.getString("value"), result.getInt("id"));
        }
        j = 0;
        for (String value : sentence) {
            if (map.containsKey(value)) {
                keys[j] = map.get(value);
            } else if (insertNew) {
                if (tableName.equals(this.EDGE_TABLE_NAME)) {
                    synchronized (this.nextEdgeKey) {
                        this.putEdge(value);
                        keys[j] = this.nextEdgeKey - 1;
                    }
                } else {
                    synchronized (this.nextMentionKey) {
                        this.putMention(value);
                        keys[j] = this.nextMentionKey - 1;
                    }
                }
            } else {
                keys[j] = null;
            }
            j++;
        }
        result.close();
        statement.close();
        return keys;
    }

    private String[] map2Values(Integer[] keys, String tableName)
            throws SQLException {
        String mapSQL = null;
        if (tableName.equals(this.EDGE_TABLE_NAME))
            mapSQL = this.GET_EDGE_VALUES_SQL;
        else
            mapSQL = this.GET_MENTION_VALUES_SQL;
        String[] values = new String[keys.length];
        int j = 1;
        String query = this.prepareINClause(mapSQL, "id", values.length);
        PreparedStatement statement = conn.prepareStatement(query);
        for (Integer key : keys) {
            statement.setInt(j, key);
            j++;
        }
        ResultSet result = statement.executeQuery();
        HashMap<Integer, String> map = new HashMap<Integer, String>();
        while (result.next()) {
            map.put(result.getInt("id"), result.getString("value"));
        }
        j = 0;
        for (Integer key : keys) {
            values[j] = map.get(key);
            j++;
        }
        result.close();
        statement.close();
        return values;
    }

    public Integer mapEdge(String value, Connection conn) throws SQLException {
        return this.getKey(value, GET_EDGE_KEY_SQL, conn);
    }

    public String mapEdge(Integer key, Connection conn) throws SQLException {
        return this.getValue(key, GET_EDGE_VALUE_SQL, conn);
    }

    public Integer mapEdge(String value) throws SQLException {
        return this.getKey(value, GET_EDGE_KEY_SQL);
    }

    public String mapEdge(Integer key) throws SQLException {
        return this.getValue(key, GET_EDGE_VALUE_SQL);
    }

    public void putEdge(String value) throws SQLException {
        this.put(this.nextEdgeKey, value, INSERT_EDGE_SQL);
        this.nextEdgeKey++;
    }

    public Integer mapMention(String value) throws SQLException {
        return this.getKey(value, GET_MENTION_KEY_SQL);
    }

    public String mapMention(Integer key) throws SQLException {
        return this.mapMention(key, this.conn);
    }

    public String mapMention(Integer key, Connection conn) throws SQLException {
        return this.getValue(key, GET_MENTION_VALUE_SQL, conn);
    }

    public void putMention(String value) throws SQLException {
        this.put(this.nextMentionKey, value, INSERT_MENTION_SQL);
        this.incrementMentionCount(this.mapMention(value));
        this.nextMentionKey++;
    }


    public TreeSet<W2CSQLCandidate> getCandidates(Integer surfaceId, int sqlLimit)
            throws SQLException {
        return this.getCandidates(surfaceId, sqlLimit, conn);
    }

    public TreeSet<W2CSQLCandidate> getCandidates(Integer surfaceId, int sqlLimit, Connection conn)
            throws SQLException {
        PreparedStatement getStatement = conn
                .prepareStatement(GET_CANDIDATES_WITH_SURFACEID_FROM_TABLE_SQL);
        getStatement.setInt(1, surfaceId);
        getStatement.setInt(2, sqlLimit);
        TreeSet<W2CSQLCandidate> candidates = new TreeSet<W2CSQLCandidate>(
                new W2CSQLCandidateExternalProbComparator());
        ResultSet r = getStatement.executeQuery();
        while (r.next()) {
            candidates.add(new W2CSQLCandidate(r.getInt("page_id"), r
                    .getDouble("prob_internal_c"), r
                    .getDouble("prob_internal_nc"), r
                    .getDouble("prob_external_c"), r
                    .getDouble("prob_external_nc"), r
                    .getInt("denom_internal_c"), r.getInt("denom_internal_nc"),
                    r.getInt("denom_external_c"),
                    r.getInt("denom_external_nc"), r.getBoolean("is_lastname"),
                    r.getBoolean("is_title"),
                    r.getDouble("prob_sort_key")
            ));
        }
        r.close();
        getStatement.close();
        return candidates;
    }

    public TreeSet<W2CSQLCandidate> getCandidates(String surfaceForm, int sqlLimit)
            throws SQLException, UnsupportedEncodingException {
        return this.getCandidates(surfaceForm, sqlLimit, conn);
    }

    public TreeSet<W2CSQLCandidate> getCandidates(String surfaceForm, int sqlLimit, Connection conn)
            throws SQLException, UnsupportedEncodingException {

        TreeSet<W2CSQLCandidate> candidates = new TreeSet<W2CSQLCandidate>(
                new W2CSQLCandidateExternalProbComparator());

        PreparedStatement getCandidatePS = this.conn
                .prepareStatement(GET_CANDIDATES_FOR_MENTION_STRING_SQL);
        getCandidatePS.setBytes(1, surfaceForm.getBytes("UTF-8"));
        getCandidatePS.setInt(2, sqlLimit);
        ResultSet r = getCandidatePS.executeQuery();

        while (r.next()) {
            candidates.add(new W2CSQLCandidate(r.getInt("page_id"), r
                    .getDouble("prob_internal_c"), r
                    .getDouble("prob_internal_nc"), r
                    .getDouble("prob_external_c"), r
                    .getDouble("prob_external_nc"), r
                    .getInt("denom_internal_c"), r.getInt("denom_internal_nc"),
                    r.getInt("denom_external_c"),
                    r.getInt("denom_external_nc"), r.getBoolean("is_lastname"),
                    r.getBoolean("is_title"), r.getDouble("prob_sort_key")
            ));
        }
        r.close();
        getCandidatePS.close();

        return candidates;
    }


    public HashMap<Integer, TreeSet<W2CSQLCandidate>> getCandidates(
            Integer[] mentionKeys, int sqlLimit) throws SQLException {
        return this.getCandidates(mentionKeys, sqlLimit, this.conn);
    }

    public HashMap<Integer, TreeSet<W2CSQLCandidate>> getCandidates(
            Integer[] mentionKeys, int sqlLimit, Connection conn) throws SQLException {
        String query = this.prepareINClause(this.GET_CANDIDATES_MENTIONS_SQL,
                "surface_id", mentionKeys.length);
        PreparedStatement getStatement = conn.prepareStatement(query);
        int i = 1;
        HashMap<Integer, TreeSet<W2CSQLCandidate>> candidates = new HashMap<Integer, TreeSet<W2CSQLCandidate>>();
        for (Integer mentionKey : mentionKeys) {
            getStatement.setInt(i++, mentionKey);
            candidates.put(mentionKey, new TreeSet<W2CSQLCandidate>(
                    new W2CSQLCandidateExternalProbComparator()));
        }
        getStatement.setInt(i, sqlLimit);
        ResultSet r = getStatement.executeQuery();
        while (r.next()) {
            candidates.get(r.getInt("surface_id")).add(
                    new W2CSQLCandidate(r.getInt("page_id"), r
                            .getDouble("prob_internal_c"), r
                            .getDouble("prob_internal_nc"), r
                            .getDouble("prob_external_c"), r
                            .getDouble("prob_external_nc"), r
                            .getInt("denom_internal_c"), r
                            .getInt("denom_internal_nc"), r
                            .getInt("denom_external_c"), r
                            .getInt("denom_external_nc"), r
                            .getBoolean("is_lastname"), r
                            .getBoolean("is_title"),
                            r.getDouble("prob_sort_key")
                    )
            );
        }
        r.close();
        getStatement.close();
        return candidates;
    }

    /*
     * insert if not exist, otherwise update all non-null value
     */
    public void putCandidate(Integer mentionKey, Integer conceptId,
                             Double probInternal, Double probInternalNonCase,
                             Double probExternal, Double probExternalNonCase,
                             Integer denomInternal, Integer denomInternalNonCase,
                             Integer denomExternal, Integer denomExternalNonCase,
                             Boolean isLastname, Boolean isTitle, Boolean skipUpdateCheck)
            throws SQLException {
        if (!skipUpdateCheck) {
            PreparedStatement getStatement = this.conn
                    .prepareStatement(GET_CANDIDATES_MENTION_CONCEPT_SQL);
            getStatement.setInt(1, mentionKey);
            getStatement.setInt(2, conceptId);
            ResultSet rs = getStatement.executeQuery();
            if (rs.next()) {
                Double oldProbInternal = (Double) (rs
                        .getObject("prob_internal_c"));
                Double oldProbInternalNonCase = (Double) (rs
                        .getObject("prob_internal_nc"));
                Double oldProbExternal = (Double) (rs
                        .getObject("prob_external_c"));
                Double oldProbExternalNonCase = (Double) (rs
                        .getObject("prob_external_nc"));
                Integer oldDenomInternal = (Integer) (rs
                        .getObject("denom_internal_c"));
                Integer oldDenomInternalNonCase = (Integer) (rs
                        .getObject("denom_internal_nc"));
                Integer oldDenomExternal = (Integer) (rs
                        .getObject("denom_external_c"));
                Integer oldDenomExternalNonCase = (Integer) (rs
                        .getObject("denom_external_nc"));
                Boolean oldIsLastname = rs.getBoolean("is_lastname");
                Boolean oldIsTitle = rs.getBoolean("is_title");

                // delete cases
                if ((isTitle != null && isTitle == false)
                        && (oldIsTitle != null && oldIsTitle == true)) {
                    this.deleteAuxCandidate(mentionKey, conceptId,
                            DELETE_TITLE_CANDIDATE_SQL);
                }
                if ((isLastname != null && isLastname == false)
                        && (oldIsLastname != null && oldIsLastname == true)) {
                    this.deleteAuxCandidate(mentionKey, conceptId,
                            DELETE_LASTNAME_CANDIDATE_SQL);
                }
                // insert cases
                if ((isTitle != null && isTitle) && (oldIsTitle == null)) {
                    this.insertAuxCandidate(mentionKey, conceptId,
                            INSERT_TITLE_CANIDATE_SQL);
                }
                if ((isLastname != null && isLastname)
                        && (oldIsLastname == null)) {
                    this.insertAuxCandidate(mentionKey, conceptId,
                            INSERT_LASTNAME_CANIDATE_SQL);
                }
                if ((probInternal != null && probInternalNonCase != null
                        && denomInternal != null && denomInternalNonCase != null)
                        && (oldProbInternal == null
                        && oldProbInternalNonCase == null
                        && oldDenomInternal == null && oldDenomInternalNonCase == null)) {
                    this.insertURLCandidate(mentionKey, conceptId,
                            probInternal, probInternalNonCase, denomInternal,
                            denomInternalNonCase, INSERT_IN_CANIDATE_SQL);
                }
                if ((probExternal != null && probExternalNonCase != null
                        && denomExternal != null && denomExternalNonCase != null)
                        && (oldProbExternal == null
                        && oldProbExternalNonCase == null
                        && oldDenomExternal == null && oldDenomExternalNonCase == null)) {
                    this.insertURLCandidate(mentionKey, conceptId,
                            probExternal, probExternalNonCase, denomExternal,
                            denomExternalNonCase, INSERT_EX_CANIDATE_SQL);
                }
                // update cases
                if ((probInternal != null && probInternalNonCase != null
                        && denomInternal != null && denomInternalNonCase != null)
                        && (oldProbInternal != null
                        && oldProbInternalNonCase != null
                        && oldDenomInternal != null && oldDenomInternalNonCase != null)
                        && (probInternal != oldProbInternal
                        || probInternalNonCase != oldProbInternalNonCase
                        || denomInternal != oldDenomInternal || denomInternalNonCase != oldDenomInternalNonCase)) {
                    this.updateURLCandidate(mentionKey, conceptId,
                            probInternal, probInternalNonCase, denomInternal,
                            denomInternalNonCase, UPDATE_IN_CANDIDATE_SQL);
                }
                if ((probExternal != null && probExternalNonCase != null
                        && denomExternal != null && denomExternalNonCase != null)
                        && (oldProbExternal != null
                        && oldProbExternalNonCase != null
                        && oldDenomExternal != null && oldDenomExternalNonCase != null)
                        && (probExternal != oldProbExternal
                        || probExternalNonCase != oldProbExternalNonCase
                        || denomExternal != oldDenomExternal || denomExternalNonCase != oldDenomExternalNonCase)) {
                    this.updateURLCandidate(mentionKey, conceptId,
                            probExternal, probExternalNonCase, denomExternal,
                            denomExternalNonCase, UPDATE_EX_CANDIDATE_SQL);
                }
                rs.close();
                getStatement.close();
                return;
            }
        }
        this.insertURLCandidate(mentionKey, conceptId, probExternal,
                probExternalNonCase, denomExternal, denomExternalNonCase,
                INSERT_EX_CANIDATE_SQL);
        this.insertURLCandidate(mentionKey, conceptId, probInternal,
                probInternalNonCase, denomInternal, denomInternalNonCase,
                INSERT_IN_CANIDATE_SQL);
        if (isTitle != null && isTitle) {
            this.insertAuxCandidate(mentionKey, conceptId,
                    INSERT_TITLE_CANIDATE_SQL);
        }
        if (isLastname != null && isLastname) {
            this.insertAuxCandidate(mentionKey, conceptId,
                    INSERT_LASTNAME_CANIDATE_SQL);
        }
    }

    public void putCandidate(Integer mentionKey, Integer conceptId,
                             Double probInternal, Double probInternalNonCase,
                             Double probExternal, Double probExternalNonCase,
                             Integer denomInternal, Integer denomInternalNonCase,
                             Integer denomExternal, Integer denomExternalNonCase,
                             Boolean isLastname, Boolean isTitle) throws SQLException {

        putCandidate(mentionKey, conceptId, probInternal, probInternalNonCase,
                probExternal, probExternalNonCase, denomInternal,
                denomInternalNonCase, denomExternal, denomExternalNonCase,
                isLastname, isTitle, false);
    }

    private void updateURLCandidate(Integer mentionKey, Integer conceptId,
                                    Double prob, Double probNonCase, Integer denom,
                                    Integer denomNonCase, String updateSQL) throws SQLException {
        PreparedStatement updateCandidatePS = this.conn
                .prepareStatement(updateSQL);
        updateCandidatePS.setObject(1, prob, java.sql.Types.DOUBLE);
        updateCandidatePS.setObject(2, probNonCase, java.sql.Types.DOUBLE);
        updateCandidatePS.setObject(3, denom, java.sql.Types.INTEGER);
        updateCandidatePS.setObject(4, denomNonCase, java.sql.Types.INTEGER);
        updateCandidatePS.setInt(5, mentionKey);
        updateCandidatePS.setLong(6, conceptId);
        updateCandidatePS.executeUpdate();
        updateCandidatePS.close();
    }

    private void insertURLCandidate(Integer mentionKey, Integer conceptId,
                                    Double prob, Double probNonCase, Integer denom,
                                    Integer denomNonCase, String insertSQL) throws SQLException {
        PreparedStatement insertCandidatePS = this.conn
                .prepareStatement(insertSQL);
        insertCandidatePS.setInt(1, mentionKey);
        insertCandidatePS.setLong(2, conceptId);
        insertCandidatePS.setObject(3, prob, java.sql.Types.DOUBLE);
        insertCandidatePS.setObject(4, probNonCase, java.sql.Types.DOUBLE);
        insertCandidatePS.setObject(5, denom, java.sql.Types.INTEGER);
        insertCandidatePS.setObject(6, denomNonCase, java.sql.Types.INTEGER);
        insertCandidatePS.executeUpdate();
        insertCandidatePS.close();
    }

    private void insertAuxCandidate(Integer mentionKey, Integer conceptId,
                                    String insertSQL) throws SQLException {
        PreparedStatement insertCandidatePS = this.conn
                .prepareStatement(insertSQL);
        insertCandidatePS.setInt(1, mentionKey);
        insertCandidatePS.setLong(2, conceptId);
        insertCandidatePS.executeUpdate();
        insertCandidatePS.close();
    }

    private void deleteAuxCandidate(Integer mentionKey, Integer conceptId,
                                    String deleteSQL) throws SQLException {
        PreparedStatement insertCandidatePS = this.conn
                .prepareStatement(deleteSQL);
        insertCandidatePS.setInt(1, mentionKey);
        insertCandidatePS.setLong(2, conceptId);
        insertCandidatePS.executeUpdate();
        insertCandidatePS.close();
    }

    private Integer getKey(String value, String sql) throws SQLException {
        return this.getKey(value, sql, this.conn);
    }

    protected Integer getKey(String value, String sql, Connection conn) throws SQLException {
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setString(1, value);
        ResultSet result = statement.executeQuery();
        Integer key = null;
        while (result.next()) {
            key = result.getInt("id");
        }
        result.close();
        statement.close();
        return key;
    }

    private String getValue(Integer key, String sql) throws SQLException {
        return this.getValue(key, sql, this.conn);
    }

    private String getValue(Integer key, String sql, Connection conn) throws SQLException {
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setInt(1, key);
        ResultSet result = statement.executeQuery();
        String value = null;
        while (result.next()) {
            value = result.getString("value");
        }
        result.close();
        statement.close();
        return value;
    }

    private void put(int key, String value, String sql) throws SQLException {
        if (value == null)
            throw new NullPointerException("'value' cannot be null");
        PreparedStatement statement = this.conn.prepareStatement(sql);
        statement.setInt(1, key);
        statement.setString(2, value);
        statement.executeUpdate();
        statement.close();
    }

    private String prepareINClause(String query, String whereFieldName,
                                   int numArgs) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < numArgs; i++) {
            builder.append(" " + whereFieldName + " = ? OR");
        }

        String args = builder.delete(builder.length() - 2, builder.length())
                .toString();
        String q = query.replaceAll("\\[ORARGS\\]", args);
        return q;
    }

    private int getLatestKey(String tableName) throws SQLException {
        String query = "SELECT MAX(`id`) as `max_key` FROM `" + tableName
                + "`;";
        PreparedStatement p = conn.prepareStatement(query);
        ResultSet result = p.executeQuery();
        int max = 1;
        while (result.next()) {
            max = result.getInt("max_key");
        }
        p.close();
        result.close();
        return max;
    }

    public void addLastNameMention(String lastName, String pageTitle)
            throws SQLException, UnsupportedEncodingException {

        Integer surfaceId = this.mapMention(lastName);
        if (surfaceId == null) {
            this.putMention(lastName);
            surfaceId = this.mapMention(lastName);
        }

        PreparedStatement lastNameInsPS = this.conn
                .prepareStatement(INSERT_LASTNAME_SQL);
        lastNameInsPS.setInt(1, surfaceId);
        lastNameInsPS.setBytes(2, pageTitle.getBytes("UTF-8"));
        lastNameInsPS.execute();
        lastNameInsPS.close();
    }

    public void incrementMentionCount(Integer surfaceID) throws SQLException {

        PreparedStatement insCountPS = this.conn
                .prepareStatement(INSERT_MENTION_COUNT);
        insCountPS.setInt(1, surfaceID);
        insCountPS.execute();

        insCountPS = this.conn.prepareStatement(INCREMENT_MENTION_COUNT);
        insCountPS.setInt(1, surfaceID);
        insCountPS.execute();

        insCountPS.close();

    }

    public Connection getConn() {
        return conn;
    }

    public void setConn(Connection conn) {
        this.conn = conn;
    }

    public final String EDGE_TABLE_NAME = "w2ctrie_edge_map";
    public final String MENTION_TABLE_NAME = "w2ctrie_surface_forms_map";
    public final String INTERNAL_CANDIDATE_TABLE_NAME = "w2ctrie_internal_url_candidate";
    public final String EXTERNAL_CANDIDATE_TABLE_NAME = "w2ctrie_external_url_candidate";
    public final String TITLE_CANDIDATE_TABLE_NAME = "w2ctrie_title_candidate";
    public final String LASTNAME_CANDIDATE_TABLE_NAME = "w2ctrie_lastname_candidate";
    public final String JOINT_CANDIDATE_CACHE_TABLE_NAME = "w2ctrie_joint_candidate_table";
    protected final String GET_EDGE_KEY_SQL = "SELECT `id` FROM `"
            + EDGE_TABLE_NAME + "` WHERE `value` = ? LIMIT 1";
    private final String GET_EDGE_VALUE_SQL = "SELECT CAST(`value` AS CHAR) as `value` FROM `"
            + EDGE_TABLE_NAME + "` WHERE `id` = ? LIMIT 1";
    private final String GET_EDGE_KEYS_SQL = "SELECT `id`, CAST(`value` AS CHAR) as `value` FROM `"
            + EDGE_TABLE_NAME + "` WHERE [ORARGS]";
    private final String GET_EDGE_VALUES_SQL = "SELECT `id`, CAST(`value` AS CHAR) as `value` FROM `"
            + EDGE_TABLE_NAME + "` WHERE [ORARGS]";
    private final String INSERT_EDGE_SQL = "INSERT IGNORE INTO `"
            + EDGE_TABLE_NAME + "` (`id`, `value`) VALUES (?, ?);";
    private final String GET_MENTION_KEY_SQL = "SELECT `id` FROM `"
            + MENTION_TABLE_NAME + "` WHERE `value` = ? LIMIT 1";
    private final String GET_MENTION_VALUE_SQL = "SELECT CAST(`value` AS CHAR) as `value` FROM `"
            + MENTION_TABLE_NAME + "` WHERE `id` = ? LIMIT 1";
    private final String GET_MENTION_KEYS_SQL = "SELECT `id`, CAST(`value` AS CHAR) as `value` FROM `"
            + MENTION_TABLE_NAME + "` WHERE [ORARGS]";
    private final String GET_MENTION_VALUES_SQL = "SELECT `id`, CAST(`value` AS CHAR) as `value` FROM `"
            + MENTION_TABLE_NAME + "` WHERE [ORARGS]";
    private final String INSERT_MENTION_SQL = "INSERT INTO `"
            + MENTION_TABLE_NAME + "` (`id`, `value`) VALUES (?, ?);";
    private final String GET_CANDIDATES_FOR_MENTION_STRING_SQL = "SELECT * FROM "
            + JOINT_CANDIDATE_CACHE_TABLE_NAME
            + ", "
            + MENTION_TABLE_NAME
            + " WHERE surface_id = id AND value = ? "
            + "ORDER BY prob_sort_key DESC LIMIT ?";
    private final String GET_CANDIDATES_MENTIONS_SQL = "SELECT * FROM `"
            + JOINT_CANDIDATE_CACHE_TABLE_NAME + "` WHERE [ORARGS]"
            + " ORDER BY prob_sort_key DESC LIMIT ?";

    private final String GET_CANDIDATES_WITH_SURFACEID_FROM_TABLE_SQL = "SELECT * FROM `"
            + JOINT_CANDIDATE_CACHE_TABLE_NAME + "` WHERE surface_id = ?"
            + " ORDER BY prob_sort_key DESC LIMIT ?";

    private final String GET_CANDIDATES_MENTION_CONCEPT_SQL = "SELECT * FROM `"
            + JOINT_CANDIDATE_CACHE_TABLE_NAME
            + "` WHERE `surface_id` = ? AND `page_id` = ?;";
    private final String INSERT_EX_CANIDATE_SQL = "INSERT INTO `"
            + EXTERNAL_CANDIDATE_TABLE_NAME + "` (`surface_id`, `page_id`, "
            + "`prob_c`, `prob_nc`, " + "`denom_c`, `denom_nc` "
            + " ) VALUES (?,?,?,?,?,?);";
    private final String INSERT_IN_CANIDATE_SQL = "INSERT INTO `"
            + INTERNAL_CANDIDATE_TABLE_NAME + "` (`surface_id`, `page_id`, "
            + "`prob_c`, `prob_nc`, " + "`denom_c`, `denom_nc` "
            + " ) VALUES (?,?,?,?,?,?);";
    private final String INSERT_TITLE_CANIDATE_SQL = "INSERT INTO `"
            + TITLE_CANDIDATE_TABLE_NAME + "` (`surface_id`, `page_id` "
            + " ) VALUES (?,?);";
    private final String INSERT_LASTNAME_CANIDATE_SQL = "INSERT INTO `"
            + LASTNAME_CANDIDATE_TABLE_NAME + "` (`surface_id`, `page_id` "
            + " ) VALUES (?,?);";
    private final String UPDATE_EX_CANDIDATE_SQL = "UPDATE `"
            + EXTERNAL_CANDIDATE_TABLE_NAME + "` SET" + " `prob_c` = ?, "
            + " `prob_nc` = ?, " + " `denom_c` = ?, " + " `denom_nc` = ? "
            + "WHERE `surface_id` = ? AND `page_id` = ?;";
    private final String UPDATE_IN_CANDIDATE_SQL = "UPDATE `"
            + INTERNAL_CANDIDATE_TABLE_NAME + "` SET" + " `prob_c` = ?, "
            + " `prob_nc` = ?, " + " `denom_c` = ?, " + " `denom_nc` = ? "
            + "WHERE `surface_id` = ? AND `page_id` = ?;";
    private final String DELETE_TITLE_CANDIDATE_SQL = "DELETE FROM `"
            + TITLE_CANDIDATE_TABLE_NAME
            + "` WHERE `surface_id` = ? AND `page_id` = ?;";
    private final String DELETE_LASTNAME_CANDIDATE_SQL = "DELETE FROM `"
            + LASTNAME_CANDIDATE_TABLE_NAME
            + "` WHERE `surface_id` = ? AND `page_id` = ?;";
    private final String INSERT_LASTNAME_SQL = "INSERT IGNORE INTO "
            + LASTNAME_CANDIDATE_TABLE_NAME
            + " SELECT ? , page_id FROM page WHERE page_title = ? AND page_namespace = 0";
    private final String INSERT_MENTION_COUNT = "INSERT IGNORE INTO w2ctrie_surface_count (id , count) VALUES (? , 0)";
    private final String INCREMENT_MENTION_COUNT = "UPDATE w2ctrie_surface_count SET count = count +1 WHERE id = ? ";
}
