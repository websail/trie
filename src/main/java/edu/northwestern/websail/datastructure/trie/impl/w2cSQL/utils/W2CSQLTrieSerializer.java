package edu.northwestern.websail.datastructure.trie.impl.w2cSQL.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Queue;

import edu.northwestern.websail.datastructure.trie.ds.Node;
import edu.northwestern.websail.datastructure.trie.ds.Trie;
import edu.northwestern.websail.datastructure.trie.ds.TrieEdge;
import edu.northwestern.websail.datastructure.trie.impl.w2cSQL.W2CSQLMentionNode;
import edu.northwestern.websail.datastructure.trie.impl.w2cSQL.W2CSQLTrie;

public class W2CSQLTrieSerializer {
	public static void writeToFile(BufferedWriter out, W2CSQLTrie w2cSQLTrie,
			Connection conn) throws IOException, SQLException {

		Trie<Byte, Integer, W2CSQLMentionNode> trie = w2cSQLTrie.getTrie();
		Node<Byte, Integer, W2CSQLMentionNode> root = trie.getRoot();
		ArrayList<TrieEdge> edgeList = new ArrayList<TrieEdge>();

		System.out.println("Number of nodes = " + trie.getNumNodes());
		out.write("==Nodes==\n");
		dfsWriteTrieNodes(out, root, edgeList, conn);

		out.write("==Edges==\n");
		for (TrieEdge e : edgeList) {
			out.write(e.getId() + "\t" + e.getParentId() + "\t"
					+ e.getEdgeIdentifier() + "\n");
		}
		out.flush();
		out.close();
	}

	private static void dfsWriteTrieNodes(BufferedWriter out,
			Node<Byte, Integer, W2CSQLMentionNode> root,
			ArrayList<TrieEdge> edgeList, Connection conn) throws IOException,
			SQLException {

		Queue<Node<Byte, Integer, W2CSQLMentionNode>> queue = new LinkedList<Node<Byte, Integer, W2CSQLMentionNode>>();
		HashMap<Integer, Node<Byte, Integer, W2CSQLMentionNode>> children;
		Node<Byte, Integer, W2CSQLMentionNode> n;
		queue.add(root);

		while (!queue.isEmpty()) {
			n = queue.poll();
			if (n.getEndpoint() != null) {
				n.getEndpoint()
						.setNumberOfConcepts(
								queryNumConcepts(n.getEndpoint()
										.getMentionKeys(), conn));
				n.getEndpoint().setNumberOfLinks(
						queryNumLinks(n.getEndpoint().getMentionKeys(), conn));
			}
			out.write(n.toString() + "\n");
			children = n.getChildren();
			for (Entry<Integer, Node<Byte, Integer, W2CSQLMentionNode>> e : children
					.entrySet()) {
				edgeList.add(new TrieEdge(e.getValue().getId(), n.getId(), e
						.getKey().toString()));
				queue.add(e.getValue());
			}
		}
	}

	private static String numConceptsQuery = "SELECT COUNT(DISTINCT page_id) AS num FROM w2ctrie_surface_page_uniq WHERE surface_id IN ";

	private static int queryNumConcepts(HashSet<Integer> surfaceIds,
			Connection conn) throws SQLException {
		StringBuilder sb = new StringBuilder();
		sb.append(numConceptsQuery);
		sb.append('(');

		for (Integer id : surfaceIds) {
			sb.append(id);
			sb.append(',');
		}

		sb.deleteCharAt(sb.length() - 1);
		sb.append(')');
		sb.append(';');

		PreparedStatement ps = conn.prepareStatement(sb.toString());
		ResultSet rs = ps.executeQuery();
		int count = 0;
		while (rs.next()) {
			count = rs.getInt("num");
			break;
		}
		rs.close();
		ps.close();
		return count;
	}

	private static String numLinksQuery = "SELECT SUM(num) as num_links "
			+ "FROM  (" + " SELECT surface_id, MAX(denom_c) as num "
			+ " FROM w2ctrie_internal_url_candidate ic "
			+ " WHERE ic.surface_id IN  ( @ARGS@ ) GROUP BY surface_id"
			+ ") a;";

	private static int queryNumLinks(HashSet<Integer> surfaceIds,
			Connection conn) throws SQLException {
		StringBuilder sb = new StringBuilder();

		for (Integer id : surfaceIds) {
			sb.append(id);
			sb.append(',');
		}

		sb.deleteCharAt(sb.length() - 1);
		String query = numLinksQuery.replace("@ARGS@", sb.toString());
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		int count = 0;
		while (rs.next()) {
			count = rs.getInt("num_links");
			break;
		}
		rs.close();
		ps.close();
		return count;
	}

	public static W2CSQLTrie readFromFile(BufferedReader in, Connection conn)
			throws IOException, SQLException {
		Trie<Byte, Integer, W2CSQLMentionNode> trie = loadTrieFromFile(in, conn);
		W2CSQLTrie t = new W2CSQLTrie(conn);
		t.setTrie(trie);
		return t;
	}

	public static W2CSQLTrie readFromFile(BufferedReader in, Connection conn,
			Connection[] connPool, String surfaceMapFile, int cacheSize,
			int sqlLimit) throws IOException, SQLException {
		Trie<Byte, Integer, W2CSQLMentionNode> trie = loadTrieFromFile(in, conn);
		W2CSQLTrie t = new W2CSQLTrie(conn, connPool, surfaceMapFile,
				cacheSize, sqlLimit);
		t.setTrie(trie);
		return t;
	}

	public static W2CSQLTrie readFromFile(BufferedReader in, Connection conn,
			Connection[] connPool, HashMap<String, Integer> edgeCache,
			String[] surfaceIdCache, int cacheSize, int sqlLimit)
			throws IOException, SQLException {
		Trie<Byte, Integer, W2CSQLMentionNode> trie = loadTrieFromFile(in, conn);
		W2CSQLTrie t = new W2CSQLTrie(conn, connPool, edgeCache,
				surfaceIdCache, cacheSize, sqlLimit);
		t.setTrie(trie);
		return t;
	}

	private static Trie<Byte, Integer, W2CSQLMentionNode> loadTrieFromFile(
			BufferedReader in, Connection conn) throws IOException {
		HashMap<Integer, Node<Byte, Integer, W2CSQLMentionNode>> nodes = new HashMap<Integer, Node<Byte, Integer, W2CSQLMentionNode>>();

		String line = "";
		// Read all nodes and mentions + candidates
		while ((line = in.readLine()) != null) {
			if (line.equalsIgnoreCase("==Edges==")) {
				break;
			} else if (line.equalsIgnoreCase("==Nodes=="))
				continue;

			String[] parts = line.split("\t");

			String val = new String(parts[1]);
			Node<Byte, Integer, W2CSQLMentionNode> n = null;
			if (val.equals("null")) {
				n = new Node<Byte, Integer, W2CSQLMentionNode>(
						Integer.valueOf(parts[0]), null);
			} else {
				n = new Node<Byte, Integer, W2CSQLMentionNode>(
						Integer.valueOf(parts[0]), Byte.valueOf(val));
			}
			if (parts[2].equalsIgnoreCase("true")) {

				W2CSQLMentionNode mention = new W2CSQLMentionNode();

				for (int i = 3; i < parts.length; i++) {

					String key = parts[i];
					if (key.startsWith(":")) {
						String[] nums = key.split(":");
						mention.setNumberOfConcepts(Integer.valueOf(nums[1]));
						mention.setNumberOfLinks(Integer.valueOf(nums[2]));
					} else {
						mention.addMention(Integer.valueOf(key));
					}
				}
				n.setEndpoint(mention);
			}

			nodes.put(n.getId(), n);
		}

		// Read all Edges
		int childId = 0;
		int parentId = 0;
		String edgeIdentifier = "";
		while ((line = in.readLine()) != null) {

			String[] parts = line.split("\t");
			childId = Integer.valueOf(parts[0]);
			parentId = Integer.valueOf(parts[1]);
			edgeIdentifier = parts[2];

			nodes.get(childId).setParent(nodes.get(parentId));
			nodes.get(parentId).addChild(Integer.valueOf(edgeIdentifier),
					nodes.get(childId));

		}

		Trie<Byte, Integer, W2CSQLMentionNode> trie = new Trie<Byte, Integer, W2CSQLMentionNode>();
		trie.setRoot(nodes.get(0));
		trie.setNumNodes(nodes.size());
		in.close();
		return trie;
	}
}
