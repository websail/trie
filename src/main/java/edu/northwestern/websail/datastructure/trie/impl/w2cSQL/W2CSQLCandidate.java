package edu.northwestern.websail.datastructure.trie.impl.w2cSQL;

import java.io.Serializable;

public class W2CSQLCandidate implements Serializable,
        Comparable<W2CSQLCandidate> {

    private static final long serialVersionUID = 7036190317463499670L;
    private Integer conceptId;
    private Integer denomInternal;
    private Integer denomInternalNonCase;
    private Integer denomExternal;
    private Integer denomExternalNonCase;
    private Double probInternal;
    private Double probInternalNonCase;
    private Double probExternal;
    private Double probExternalNonCase;
    private Boolean isTitle;
    private Boolean isLastname;
    private Double probSortKey;

    public W2CSQLCandidate(Integer conceptId, Double probInternal,
                           Double probInternalNonCase, Double probExternal,
                           Double probExternalNonCase, Integer denomInternal,
                           Integer denomInternalNonCase, Integer denomExternal,
                           Integer denomExternalNonCase, Boolean isLastname, Boolean isTitle, Double probSortKey) {
        super();
        this.conceptId = conceptId;
        this.denomInternal = denomInternal;
        this.denomInternalNonCase = denomInternalNonCase;
        this.denomExternal = denomExternal;
        this.denomExternalNonCase = denomExternalNonCase;
        this.probInternal = probInternal;
        this.probInternalNonCase = probInternalNonCase;
        this.probExternal = probExternal;
        this.probExternalNonCase = probExternalNonCase;
        this.isLastname = isLastname;
        this.isTitle = isTitle;
        this.probSortKey = probSortKey;
    }

    public W2CSQLCandidate(String serializedString) {
        String[] parts = serializedString.split(":");
        this.setConceptId(Integer.valueOf(parts[0]));
        this.setProbInternal(Double.valueOf(parts[1]));
        this.setProbInternalNonCase(Double.valueOf(parts[2]));
        this.setProbExternal(Double.valueOf(parts[3]));
        this.setProbExternalNonCase(Double.valueOf(parts[4]));

        this.setDenomInternal(Integer.valueOf(parts[5]));
        this.setDenomInternalNonCase(Integer.valueOf(parts[6]));
        this.setDenomExternal(Integer.valueOf(parts[7]));
        this.setDenomExternalNonCase(Integer.valueOf(parts[8]));

        this.setIsTitle(Boolean.valueOf(parts[9]));
        this.setIsLastname(Boolean.valueOf(parts[10]));
        this.setProbSortKey(Double.valueOf(parts[11]));

    }

    @Override
    public String toString() {
        return conceptId + ":" + probInternal + ":" + probInternalNonCase + ":"
                + probExternal + ":" + probExternalNonCase + ":"
                + denomInternal + ":" + denomInternalNonCase + ":"
                + denomExternal + ":" + denomExternalNonCase + ":" + isTitle
                + ":" + isLastname + ":" + probSortKey;

    }

    // getters and setters
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public Integer getConceptId() {
        return conceptId;
    }

    public Integer getDenomInternal() {
        return denomInternal;
    }

    public void setDenomInternal(Integer denomInternal) {
        this.denomInternal = denomInternal;
    }

    public Integer getDenomInternalNonCase() {
        return denomInternalNonCase;
    }

    public void setDenomInternalNonCase(Integer denomInternalNonCase) {
        this.denomInternalNonCase = denomInternalNonCase;
    }

    public Integer getDenomExternal() {
        return denomExternal;
    }

    public void setDenomExternal(Integer denomExternal) {
        this.denomExternal = denomExternal;
    }

    public Integer getDenomExternalNonCase() {
        return denomExternalNonCase;
    }

    public void setDenomExternalNonCase(Integer denomExternalNonCase) {
        this.denomExternalNonCase = denomExternalNonCase;
    }

    public void setConceptId(Integer conceptId) {
        this.conceptId = conceptId;
    }

    public Double getProbInternal() {
        return probInternal;
    }

    public Double getProbInternalNonCase() {
        return probInternalNonCase;
    }

    public Double getProbExternal() {
        return probExternal;
    }

    public Double getProbExternalNonCase() {
        return probExternalNonCase;
    }

    public Boolean getIsTitle() {
        return isTitle;
    }

    public Boolean getIsLastname() {
        return isLastname;
    }

    public void setProbInternal(Double probInternal) {
        this.probInternal = probInternal;
    }

    public void setProbInternalNonCase(Double probInternalNonCase) {
        this.probInternalNonCase = probInternalNonCase;
    }

    public void setProbExternal(Double probExternal) {
        this.probExternal = probExternal;
    }

    public void setProbExternalNonCase(Double probExternalNonCase) {
        this.probExternalNonCase = probExternalNonCase;
    }

    public void setIsTitle(Boolean isTitle) {
        this.isTitle = isTitle;
    }

    public void setIsLastname(Boolean isLastname) {
        this.isLastname = isLastname;
    }

    public Double getProbSortKey() {
        return probSortKey;
    }

    public void setProbSortKey(Double probSortKey) {
        this.probSortKey = probSortKey;
    }

    public int compareTo(W2CSQLCandidate o) {
        int sign = (int) Math.signum(this.getProbExternal()
                - o.getProbExternal());

        if (sign == 0) {
            return this.getConceptId() - o.getConceptId();
        } else {
            return sign;
        }
    }
}
