package edu.northwestern.websail.datastructure.trie.ds.test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.TreeSet;

import edu.northwestern.websail.datastructure.trie.impl.w2cSQL.W2CSQLCandidate;
import edu.northwestern.websail.datastructure.trie.impl.w2cSQL.W2CSQLTrie;
import edu.northwestern.websail.datastructure.trie.impl.w2cSQL.utils.W2CSQLTrieSerializer;
import edu.northwestern.websail.utils.MySQLQueryHandler;

public class SerializedW2CSQLTrieTest {

	public static void main(String[] args) throws InstantiationException,
			IllegalAccessException, ClassNotFoundException, SQLException,
			IOException {
		MySQLQueryHandler handler = new MySQLQueryHandler(
				"downey-n2.cs.northwestern.edu", "apr29", "kgridUser",
				"kgridPwd");
		String serializedTrie = "/websail/common/wikification/data/en/trie/trieSerialized-29Apr.dat";
		Connection conn = handler.getConn();
		BufferedReader in = new BufferedReader(new FileReader(serializedTrie));
		W2CSQLTrie trie = W2CSQLTrieSerializer.readFromFile(in, conn);
		String[] surface = {"spy"};
		HashMap<String, TreeSet<W2CSQLCandidate>> res = trie.getAllCandidates(surface, 200);
		System.out.println(res);
		in.close();
		conn.close();

	}

}
