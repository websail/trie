package edu.northwestern.websail.datastructure.trie.impl.words2concepts;

public class W2CConstants {

	public static final Byte INTERNAL_LINKS = (byte) (0xff);
	public static final Byte EXTERNAL_LINKS = (byte) (0x0f);

}
