package edu.northwestern.websail.datastructure.trie.impl.words2concepts;

import java.io.Serializable;

public class W2CCandidate implements Serializable {

	private static final long serialVersionUID = -7207237999227605528L;
	Integer pageID;
	Double probabilityPageGivenMention;
	Byte source;

	public W2CCandidate(Integer pageID,
			Double probabilityPageGivenMention, Byte source) {
		super();
		this.pageID = pageID;
		this.probabilityPageGivenMention = probabilityPageGivenMention;
		this.source = source;
	}

	public W2CCandidate(String serializedCandidateStr) {
		String[] parts = serializedCandidateStr.split(":");
		setPageID(Integer.valueOf(parts[0]));
		setProbabilityPageGivenMention(Double.valueOf(parts[1]));
		setSource(Byte.valueOf(parts[2]));
	}

	public Integer getPageID() {
		return pageID;
	}

	public void setPageID(Integer pageID) {
		this.pageID = pageID;
	}

	public Double getProbabilityPageGivenMention() {
		return probabilityPageGivenMention;
	}

	public void setProbabilityPageGivenMention(
			Double probabilityPageGivenMention) {
		this.probabilityPageGivenMention = probabilityPageGivenMention;
	}

	public Byte getSource() {
		return source;
	}

	public void setSource(Byte source) {
		this.source = source;
	}

	public String toString() {
		return pageID + ":" + probabilityPageGivenMention + ":" + source;
	}
}
