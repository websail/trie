package edu.northwestern.websail.datastructure.trie.ds;

/*
 * 
 * V - Type of value held in the node
 * E - Edge identifier type
 * C - Type of end-point
 * 
 */

public class Trie<V, E, C> {

	Node<V, E, C> root;
	private Integer NUM_NODES = 0;

	public Trie() {
		root = new Node<V, E, C>(NUM_NODES);
	}

	public Node<V, E, C> insertNodes(V[] values, E[] edgeIdentifiers) {

		int len = edgeIdentifiers.length;

		Node<V, E, C> current = root;

		int i = 0;

		while (i < len && current.hasChild(edgeIdentifiers[i])) {
			current = current.getChild(edgeIdentifiers[i]);
			i++;
		}

		for (; i < values.length; i++) {
			Node<V, E, C> child = new Node<V, E, C>(++NUM_NODES, values[i]);
			current.addChild(edgeIdentifiers[i], child);
			current = child;
		}

		return current;

	}

	public Node<V, E, C> getNode(E[] edgeIdentifiers) {

		Node<V, E, C> current = root;
		int len = edgeIdentifiers.length;
		int i = 0;

		while (i < len && current.hasChild(edgeIdentifiers[i])) {
			current = current.getChild(edgeIdentifiers[i]);
			i++;
		}

		if (i == len)
			return current;
		else
			return null;
	}

	public Node<V, E, C> getRoot() {
		return root;
	}

	public void setRoot(Node<V, E, C> root) {
		this.root = root;
	}

	public Integer getNumNodes() {
		return NUM_NODES;
	}

	public void setNumNodes(Integer nUM_NODES) {
		NUM_NODES = nUM_NODES;
	}

}
