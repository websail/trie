package edu.northwestern.websail.datastructure.trie.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import edu.northwestern.websail.datastructure.trie.ds.Trie;

public class TrieTest {

	public TrieTest() {
		
	}
	
	@Test
	public void testDSTrie(){
		Trie<String, String, String> t = new Trie<String, String, String>();

		t.insertNodes(new String[] { "List", "of", "U.S", "Presidents" },
				new String[] { "list", "of", "u.s", "presidents" });
		t.insertNodes(new String[] { "List", "of", "Indian", "Presidents" },
				new String[] { "list", "of", "indian", "presidents" });
		t.insertNodes(new String[] { "List", "of", "U.S", "Army" },
				new String[] { "list", "of", "u.s", "army" });

		assertEquals("test node id",(Integer) 7, t.getNode(
				new String[] { "list", "of", "u.s", "army" }).getId());
		assertEquals("test children", "{of=2	of	false}", t.getNode(new String[]{"list"}).getChildren().toString());
	}

}
